<?php

/**
 * Plugin Name: Pronamic Taxonomy to P2P
 * Description: Used to convert taxonomys to a post2post connection
 * Author: Pronamic
 * Author URI: http://pronamic.nl
 */

add_action( 'admin_init', 'pronamic_convert_taxonomy_to_p2p_emg' );
function pronamic_convert_taxonomy_to_p2p_emg() {
	if ( ! filter_has_var( INPUT_GET, 'fix_p2p' ) )
		return;
	
	// get wp query
	$file_query = new WP_Query( array(
		'post_type' => 'file',
		'nopaging' => true
	) );
	
	$file_term_ids = array();
	foreach ( $file_query->posts as $file_post ) {
		$term_id = get_post_meta( $file_post->ID, '_file_category_id', true );
		
		if ( ! empty( $term_id ) )
			$file_term_ids[$file_post->ID] = $term_id;
	}
	
	
	$article_query = new WP_Query( array(
		'post_type' => 'article',
		'nopaging' => true
	) );
	
	// post to post type
	$post2post_type = p2p_type( 'articles_to_files' );
	
	foreach ( $article_query->posts as $post ) {
		
		$article_terms = get_the_terms( $post->ID, 'file_category' );
		foreach ( $article_terms as $article_term ) {
			
			if ( in_array( $article_term->term_id, $file_term_ids ) ) {
				
				$term_post_id = array_search( $article_term->term_id, $file_term_ids );
				
				$post2post_type->connect( $post->ID, $term_post_id, array(
					'date' => current_time( 'mysql' )
				) );
			}
		}
	}
}